# Проект: «Разработка сервисов в центре студенческих проектов»

В результате проведенной работы, нами был создан сервис визуализации данных, представленных в табличном виде. С его помощью можно просто и быстро представить файлы с наиболее популярными табличными расширениями .xls и .xlsx в виде удобных графиков. Надеемся, что благодаря данному сервису, работа его пользователей с таблицами будет упрощена, так как появляется возможность увидеть то, как загружаемые данные изменяются и как они связаны между собой. Что весьма удобно, у каждого пользователя есть свой личный кабинет, позволяющий ему не отвлекаться на поиск нужного файла в общем пуле, а работать только со своими.

Проект был разработан в рамках требований задания по закрытию задолженности, выданного куратором. Работа была ему продемонстрирована и получена положительная оценка.

## Участники

| Учебная группа     | Имя пользователя | ФИО                 |
|--------------------|------------------|---------------------|
| 171-341            | @ArtAni          | Аникеец А.В         |
| 171-341            | @DENESTER        | Нестеров Д.И.       |
| 171-341            | @StigS           | Другов М.С.         |


## Личный вклад участников

### Аникеец А.В.

Осуществил наибольшую часть работы в части фронт- и бэкенд-разработки. В процессе разработки, тестировал работоспособность сервиса и осуществлял отладку. Помогал в оформлении документации к проекту.
Итоговое время работы - 33 часа.

### Нестеров Д.И.

Учавствовал во фронт- и бэкенд-разработке. В процессе разработки, также тестировал работоспособность сервиса и осуществлял отладку. Помогал в оформлении документации проекта.Сделал презентацию и видео-презентацию к проекту
Итоговое время работы - 32 часа.

### Другов М.С.

Во время работы над проектом коммуницировал с заказчиком и осуществлял обзор аналогичных проектов, выявления дополнительные требования. Определил целевые действия и структуру сайта. Учавствовал во фронт- и бэкенд-разработке. В процессе разработки, также тестировал работоспособность сервиса и осуществлял отладку. В заключение, при помощи других участников, была оформлена документация к проекту.
Итоговое время работы - 32 часа.

### Сайт - http://exel.std-592.ist.mospolytech.ru/

### Документация

https://drive.google.com/drive/folders/1qea17oc9yHXhRtH8K1xvTDpZMLnob_p3?usp=sharing