@extends('layouts.app')
@section('content')

	<div class="container-fluid-auto text-center">
		<div class="row text-center" style="margin-right: 0px!important; margin-left: 0px!important;">

		<div class="col-12" style="padding-right: 0px!important; padding-left: 0px!important;" id="accordion" >
  			<div class="card border border-secondary">
    			<div class= "card-header bg-secondary" id="headingOne">
     				 <h5 class="mb-0">
        				<button class="btn btn-link bg-primary text-white border border-dark" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
         					 Инструкция по работе с сервисом
        				</button>
      				</h5>
    			</div>

			<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
      			<div class="card-body">
					<ul class="list-group">
						<li class="list-group-item active">Для работы с файлом нужно:</li>
						<li class="list-group-item">1. Предварительно, привести обрабатываемый файл к следующему виду:
						<img src="http://exe1.std-592.ist.mospolytech.ru/example.png" class="img-fluid" alt="Пример табличного файла"> </br> Красным цветом обозначается столбец, формирующий ось Ох, зелёным и синим обозначаются поля данных, относящиеся к конкретным атрибутам таблицы, которые написаны в первой строке.</li>
						<li class="list-group-item">2. Выбрать файл или файлы нажатием на соответствующую кнопку</li>
						<li class="list-group-item">3. Нажать "Загрузить"</li>
						<li class="list-group-item"> Нажатие кнопки "Посмотреть" рядом с графиком, перенаправляет пользователя на отдельную страницу с диаграммой в увеличенном виде.</li>
						<li class="list-group-item"> Для каждого графика также присутствует возможность отключения отображения любых атрибутов исходной таблицы. Осуществляется это путём нажатия на атрибут в легенде под диаграммой.</li>
					</ul>
      			</div>
   			</div>
 		</div>


			

			<form class="md-form text-center col align-self-center mt-3" method="post" action="{{ route('upload_file') }}" enctype="multipart/form-data">
			    <input name="_token" type="hidden" value="{{ csrf_token() }}">
			    <input type="hidden" name="user" value="{{ Auth::user()->id }}">

			   <div class="file-field">
			    <div class="float-left">
			      <label class="btn btn-primary"  id="fileLabel" for="someFile" onchange="test(this);">Выберите файл</label>
			      <script type="text/javascript">
			      	function test(file){
			      		
			      		alert(file[0])
			      		//document.getElementById ("fileLabel").innerHTML = x[0];
			      	}
			      	
			      </script>
			      <input id="someFile" class="form-control-file" style = "display:none"  type="file" multiple name="file[]">
			    </div>
			  </div>

			    	<button class="btn btn-primary" type="submit">Загрузить</button>
			</form>
		</div>
	</div>

    <table class="table">

        @if($files)
            <thead class="bg-info" style="border: 2px solid #ccc;">
                <th scope="col" >Список загруженных файлов</th>
                <th scope="col"></th>
                <th scope="col"></th>
            </thead>
            <tbody class="" style="background-color: #CCFFFF;">

            	<?php $count_files = 0; ?>
                @foreach ($files as $file)
                    
                	<?php
                	foreach ($userfile as $uf){
                		if ($file == $uf->filename and $uf->user == Auth::user()->id) {
                			echo '
                			<tr style="border: 3px solid #ccc" >
		                        <td>'.$file.'</td>
		                        <td>

		                        	<a href="#delite_'.$count_files.'" class="btn btn-primary" data-toggle="modal">Удалить</a>
		                        	<div id="delite_'.$count_files.'" class="modal fade">
								        <div class="modal-dialog">
								            <div class="modal-content">
								                <div class="modal-header">
								                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								                    <h4 class="modal-title">Вы уверены, что хотите удалить '.$file.'?</h4>
								                </div>
								                <!-- <div class="modal-body">
								                    Содержимое модального окна 1...
								                </div> -->
								                <div class="modal-footer">
								                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
								                    <a class="btn btn-primary" href="';?>{{ route('upload_delete',['filename' => $file]) }}
								                    	<?php echo '">Удалить</a>
								                </div>
								            </div>
								        </div>
								    </div>
		                        	<!-- <a class="btn btn-primary" href="'; ?>{{ route('upload_delete',['filename' => $file]) }}
		                        		<?php echo '">Удалить</a> -->	
		                        </td>
		                        <td><a class="btn btn-primary" href="'; ?>{{ route('show_file',['filename' => $file]) }}
		                        	<?php echo '">Посмотреть</a></td>

		                    </tr>';
                		}
                	}
                	?>
                    
                    <?php $count_files++;  ?>
                @endforeach
            </tbody>
        @else
            <tr>
                <td colspan="2">Файлов нет</td>
            </tr>
        @endif
    </table>
	<script type="text/javascript">
			
			<?php $count = 0 ?>
			@foreach($array as $arr)
			<?php $legend = $arr[0]; ?> 
				$(function () {
					<?php array_shift($arr) ?> 
					var x_axis<?php echo $count; ?> = <?php echo json_encode(array_column($arr, 0)); ?>;
					var data_click<?php echo $count; ?> = <?php echo json_encode(array_column($arr, 1)); ?>;
					var data_viewer<?php echo $count; ?> = <?php echo json_encode(array_column($arr, 2)); ?>;
	    			
	    			<?php $col = 1; ?>
	    			<?php foreach ($arr[0] as $a) {
	    				if (array_column($arr, $col) == null) {
	    					break;
	    				}
	    				echo "var data".$count."".$col." = ".json_encode(array_column($arr, $col)).";";
	    				$col ++;
	    				
	    			} ?>

	    			// data_click<?php echo $count; ?>.shift();
	    			// data_viewer<?php echo $count; ?>.shift();
	    			// x_axis<?php echo $count; ?>.shift();

	    			$(<?php echo "'#container".$count."'"; ?>).highcharts(
		    			{
					        chart: {
					            type: 'line'
					        },
					        title: {
					            text: '<?php echo ''.$filenames[$count]; ?>'
					        },
					        xAxis: {
					            categories: x_axis<?php echo $count; ?>
					        },
					        yAxis: {
					            title: {
					                text: ''
					            }
					        },
					        series: [
								<?php $col = 1; ?>
				    			<?php foreach ($arr[0] as $a) {
				    				if (array_column($arr, $col) == null) {
				    					break;
				    				}
				    				echo "{ data: data".$count."".$col.",";
				    				echo " name: '".$legend[$col]."'},";
				    				$col ++;	
				    			} ?>
					        ]
					    }
				    );
    			});
				<?php if ($count >= 4) {
	            	break;
	            } ?>
				<?php $count++ ?>
			@endforeach
		
	</script>

	<?php $count = 0 ?>

	<div class="container pb-3">
		<br/>
		<h2 class="text-center"></h2>
		<div class="row">
		        
		@foreach($array as $arr)
			<div class="col-md-6 border border-secondary rounded">
	            <div class="panel panel-default">
	                <div class="panel-heading"></div>
	                <div class="panel-body">
	                    <div id="<?php echo "container".$count; ?>"></div>
	                </div>
	            </div>
            </div>
            <?php if ($count >= 4) {
            	break;
            } ?>
			<?php $count++ ?>
		@endforeach
	
	    </div>
	</div>
	

	
	
@endsection